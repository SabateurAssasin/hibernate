package com.hibernate;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * Created by Theja Varikuti on 16/08/20
 */
@Entity
@Table(schema = "pa_admin", name = "sab_test")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class SabTable {

  private int steamId;
  private int kills;
  private int deaths;
  private int assists;
  private SabName playerName;
  private String game;
  private Double rating;

  @Id
  @Column(name = "id_steam")
  public int getSteamId() {
    return steamId;
  }

  public void setSteamId(int steamId) {
    this.steamId = steamId;
  }

  @Column(name = "id_kills")
  public int getKills() {
    return kills;
  }

  public void setKills(int kills) {
    this.kills = kills;
  }

  @Column(name = "id_deaths")
  public int getDeaths() {
    return deaths;
  }

  public void setDeaths(int deaths) {
    this.deaths = deaths;
  }

  @Column(name = "id_assists")
  public int getAssists() {
    return assists;
  }

  public void setAssists(int assists) {
    this.assists = assists;
  }

  public SabName getPlayerName() {
    return playerName;
  }

  public void setPlayerName(SabName playerName) {
    this.playerName = playerName;
  }

  @Column(name = "tx_game")
  public String getGame() {
    return game;
  }

  public void setGame(String game) {
    this.game = game;
  }

  @Column(name = "nu_rating")
  public Double getRating() {
    return rating;
  }

  public void setRating(Double rating) {
    this.rating = rating;
  }

  @Override
  public String toString() {
    return "SabTable{" +
        "steamId=" + steamId +
        ", kills=" + kills +
        ", deaths=" + deaths +
        ", assists=" + assists +
        ", game='" + game + '\'' +
        ", rating=" + rating +
        '}';
  }
}
