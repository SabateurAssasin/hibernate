package com.hibernate.cache;

import com.hibernate.SabTable;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * Created by Theja Varikuti on 18/08/20
 */
public class SecondLevelCache {

  public static void main(String[] args) {
    SabTable sabTable = new SabTable();

    Configuration configuration = new Configuration().configure().addAnnotatedClass(SabTable.class);

    SessionFactory sessionFactory = configuration.buildSessionFactory();
    Session session1 = sessionFactory.openSession();

    Transaction tx = session1.beginTransaction();
    sabTable = session1.get(SabTable.class, 124);
    System.out.println(sabTable);

    sabTable = session1.get(SabTable.class, 123);
    System.out.println(sabTable);
    tx.commit();
    session1.close();

    Session session2 = sessionFactory.openSession();
    Transaction tx2 = session2.beginTransaction();
    sabTable = session2.get(SabTable.class, 124);
    System.out.println(sabTable);
    tx2.commit();
    session2.close();

  }
}
