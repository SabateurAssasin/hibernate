package com.hibernate.cache;

import com.hibernate.SabTable;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * Created by Theja Varikuti on 18/08/20
 */
public class FirstLevelCache {

  public static void main(String[] args) {
    SabTable sabTable = new SabTable();

    Configuration configuration = new Configuration().configure().addAnnotatedClass(SabTable.class);

    SessionFactory sessionFactory = configuration.buildSessionFactory();
    Session session = sessionFactory.openSession();

    Transaction tx = session.beginTransaction(); // ACID properties
    sabTable = session.get(SabTable.class, 124);
    System.out.println(sabTable);

    sabTable = session.get(SabTable.class, 123);
    System.out.println(sabTable);

    // No query will be fired....Works only for a single session (in 1st level cache)
    sabTable = session.get(SabTable.class, 124);
    System.out.println(sabTable);
    tx.commit();

  }
}
