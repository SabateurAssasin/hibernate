package com.hibernate.simple;

import com.hibernate.SabTable;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * Created by Theja Varikuti on 17/08/20
 */
public class GetValues {

  public static void main(String[] args) {
    SabTable sabTable = new SabTable();

    Configuration configuration = new Configuration().configure().addAnnotatedClass(SabTable.class);

    SessionFactory sessionFactory = configuration.buildSessionFactory();
    Session session = sessionFactory.openSession();

    Transaction tx = session.beginTransaction(); // ACID properties
    sabTable = session.get(SabTable.class, 124);
    tx.commit();

    System.out.println(sabTable);
  }
}
