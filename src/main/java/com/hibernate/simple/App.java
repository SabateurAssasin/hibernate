package com.hibernate.simple;

import com.hibernate.SabName;
import com.hibernate.SabTable;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * Created by Theja Varikuti on 16/08/20
 */
public class App {

  public static void main(String[] args) {

    SabName sabName = new SabName();
    sabName.setfName("Teja");
    sabName.setlName("Varikuti");
    sabName.setmName("Noob");


    SabTable sabTable = new SabTable();
    sabTable.setSteamId(126);
    sabTable.setKills(10);
    sabTable.setAssists(10);
    sabTable.setDeaths(5);
    sabTable.setPlayerName(sabName);
    sabTable.setGame("CSGO");
    sabTable.setRating(9.0);

    Configuration configuration = new Configuration().configure().addAnnotatedClass(SabTable.class);

    SessionFactory sessionFactory = configuration.buildSessionFactory();
    Session session = sessionFactory.openSession();

    Transaction tx = session.beginTransaction(); // ACID properties
    session.save(sabTable);
    tx.commit();
  }
}
